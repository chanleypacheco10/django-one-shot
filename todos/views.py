from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import (
    CreateTodoListForm,
    UpdateTodoListForm,
    CreateTodoItemForm,
    UpdateTodoItemForm,
)


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }

    return render(request, "todos/list.html", context)


def show_todo_item(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_list_detail = TodoItem.objects.filter(list=todo_list)
    context = {
        "todo_list": todo_list,
        "todo_list_detail": todo_list_detail,
    }

    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = CreateTodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = CreateTodoListForm()

    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = UpdateTodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = UpdateTodoListForm(instance=list)

    context = {
        "form": form
    }

    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    item = TodoList.objects.get(id=id)
    if request.method == "POST":
        item.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = CreateTodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = CreateTodoItemForm()
        context = {
            "form": form
        }

        return render(request, "todos/create_item.html", context)


def update_todo_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = UpdateTodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = UpdateTodoItemForm(instance=item)

    context = {
        "form": form
    }

    return render(request, "todos/edit_item.html", context)
