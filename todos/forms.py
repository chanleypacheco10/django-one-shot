from django import forms
from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class CreateTodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ['name']


class UpdateTodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ['name']


class CreateTodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ['task', 'due_date', 'is_completed', 'list']
        widgets = {
            'due_date': forms.DateInput(attrs={'type': 'date'})
        }


class UpdateTodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ['task', 'due_date', 'is_completed', 'list']
        widgets = {
            'due_date': forms.DateInput(attrs={'type': 'date'})
        }
